/*
Navicat MySQL Data Transfer

Source Server         : 192.168.7.75
Source Server Version : 50562
Source Host           : 192.168.7.75:3306
Source Database       : wcp2op

Target Server Type    : MYSQL
Target Server Version : 50562
File Encoding         : 65001

Date: 2020-06-18 08:54:24
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for farm_doc_view_copy
-- ----------------------------
DROP TABLE IF EXISTS `farm_doc_view`;
CREATE TABLE `farm_doc_view` (
  `ID` varchar(32) NOT NULL,
  `CTIME1` varchar(16) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `CUSERNAME` varchar(64) NOT NULL,
  `DOCID` varchar(64) DEFAULT NULL,
  `CTIME2` varchar(16) NOT NULL,
  `CTIME3` varchar(16) NOT NULL,
  `CTIME4` varchar(16) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET FOREIGN_KEY_CHECKS=1;
