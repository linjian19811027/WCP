package com.farm.wda.adaptor;

import com.farm.wda.domain.DocTask;
import com.farm.wda.util.AppConfig;

import java.io.File;



public abstract class DocConvertorBase {

	public void convert(File file, String fileTypeName, File targetFile) {
		run(file, fileTypeName, targetFile);
	}

	public abstract void run(File file, String fileTypeName, File targetFile);
}
