package com.farm.wda.web.servlet;

import java.io.File;



import com.farm.core.ParameterService;
import com.farm.parameter.FarmParameterService;
import com.farm.web.task.ServletInitJobInter;
import org.apache.log4j.Logger;

import com.farm.wda.Beanfactory;

import javax.servlet.ServletContext;


public class ConvertInit implements ServletInitJobInter {

    /**
     * 任务集合
     */
    private static final long serialVersionUID = 1L;




    private final Logger log = Logger.getLogger(this.getClass());


    /**
     * 初始化容器路径
     */
    private void initWebPath() {
        ParameterService paras = FarmParameterService.getInstance();
        String path = paras.getParameter("config.doc.dir");
        if (path.startsWith("WEBROOT")) {
            path = path.replace("WEBROOT", paras.getParameter("farm.constant.webroot.path"));
        }
        Beanfactory.WEB_DIR = path;//AppConfig.getString("config.file.dir.path") .replace("WEBROOT", super.getServletContext().getRealPath("/"));
        if (File.separator.equals("/")) {
            Beanfactory.WEB_DIR = Beanfactory.WEB_DIR.replace("\\\\", "/")
                    .replace("//", "/");
        } else {
            Beanfactory.WEB_DIR = Beanfactory.WEB_DIR.replace("/", "\\")
                    .replace("\\\\", "\\");
        }
        //Beanfactory.WEB_URL = getServletContext().getContextPath();
        //log.info("初始化容器路径" + Beanfactory.WEB_DIR);
        //log.info("初始化容器URL" + Beanfactory.WEB_URL);
    }

	/**
	 * 被执行的任务
	 *
	 * @param context
	 */
	@Override
	public void execute(ServletContext context) {
		initWebPath();
		Beanfactory.startOpenOfficeServer();
	}
}
