package com.farm.wda;

import java.net.ConnectException;

import java.util.Date;

import com.farm.core.ParameterService;
import com.farm.parameter.FarmParameterService;
import org.apache.log4j.Logger;

import com.artofsolving.jodconverter.openoffice.connection.OpenOfficeConnection;
import com.artofsolving.jodconverter.openoffice.connection.SocketOpenOfficeConnection;
import com.farm.wda.adaptor.DocConvertorBase;
import com.farm.wda.domain.DocTask;


import com.farm.wda.service.FileKeyCoderInter;
import com.farm.wda.service.impl.FileKeyCoderImpl;
import com.farm.wda.util.AppConfig;
import com.farm.wda.util.ConfUtils;
import com.farm.wda.util.FileUtil;

/**
 * 服务门面类
 * 
 * @author macplus
 *
 */
public class Beanfactory {

	public static String WEB_DIR;
	public static String WEB_URL;
	private static boolean isstart;
	private static final Logger log = Logger.getLogger(Beanfactory.class);



	/**
	 * 获得文件命名文件地址服务
	 * 
	 * @return
	 */
	public static FileKeyCoderInter getFileKeyCoderImpl() {
		return new FileKeyCoderImpl();
	}

	/**
	 * openoffce服务是否启动
	 * 
	 * @return
	 */
	public static boolean isStartByOpenofficeServer() {
		boolean isStart = false;
		OpenOfficeConnection con = new SocketOpenOfficeConnection(
				Integer.valueOf(AppConfig.getString("config.openoffice.port")));
		try {
			con.connect();
			if (con.isConnected()) {
				isStart = true;
			} else {
				isStart = false;
			}
		} catch (ConnectException e1) {
			isStart = false;
		} finally {
			if (isStart) {
				con.disconnect();
			}
		}
		return isStart;
	}

	/**
	 * 启动office转换服务
	 */
	public static void startOpenOfficeServer() {
		//查看启动情况
//		C:\Users\nusbed>netstat -ano|findstr "8100"
//		TCP    127.0.0.1:8100         0.0.0.0:0              LISTENING       63096
//		TCP    172.16.90.32:58100     192.168.7.75:3306      ESTABLISHED     37688
//
//		C:\Users\nusbed>tasklist|findstr "63096"
//		soffice.bin                  63096 Console                    1     60,576 K
//
//		C:\Users\nusbed>
		Runtime runtime = Runtime.getRuntime();
		ParameterService paras = FarmParameterService.getInstance();
		String port = paras.getParameter("config.openoffice.port");
		OpenOfficeConnection con = new SocketOpenOfficeConnection(
				Integer.valueOf(port));
		boolean isStart = false;
		try {
			con.connect();
			if (con.isConnected()) {
				isStart = true;
			} else {
				isStart = false;
			}
		} catch (ConnectException e1) {
			isStart = false;
		} finally {
			if (isStart) {
				con.disconnect();
			}
		}
		if (!isStart) {
			try {
				log.info("执行启动openoffice服务...");
				//log.info(AppConfig.getString("config.server.openoffice.cmd"));
				runtime.exec(paras.getParameter("config.server.openoffice.cmd"));
			} catch (Exception e) {
				log.info("Error:openoffice服务");
			}
		} else {
			log.info("发现openoffice服务已经被启动！");
		}
	}



	/**
	 * 启动转换器
	 */
	public static void statrtConverter(final DocTask task) {
//		if (isstart == true) {
//			return;
//		}
//		isstart = true;
		final Thread t = new Thread(new Runnable() {
			public void run() {
				log.info("启动转换任务！");
				//while (true) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e1) {
					}
					//if (!WdaAppImpl.tasks.isEmpty()) {
						//DocTask task = WdaAppImpl.tasks.element();
						try {
							task.setState("2");
							task.setStime(new Date());
							FileUtil.wirteLog(task.getLogFile(), "file key is\"" + task.getKey() + "\"" + "start task!-" + task.getFileTypeName() + " to " + task.getTargetFile().getName());
							FileUtil.wirteLog(task.getLogFile(), "converting ... ...");
							//这个地方转换
							DocConvertorBase convertor = ConfUtils.getConvertor(task.getFileTypeName(),FileUtil.getExtensionName(task.getTargetFile().getName()));
							convertor.convert(task.getFile(), task.getFileTypeName(), task.getTargetFile());
							FileUtil.wirteLog(task.getLogFile(), "success!" + task.getTargetFile().getName());
						} catch (Exception e) {
							e.printStackTrace();
							FileUtil.wirteLog(task.getLogFile(),"error!" + task.getTargetFile().getName() + "  " + e.getMessage());
							log.error("转换异常" + e.getMessage());
						}
						//WdaAppImpl.tasks.remove();
					//}
				//}
			}
		});
		t.start();
	}
}
