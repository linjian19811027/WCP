package com.farm.doc.server;

import com.farm.core.auth.domain.LoginUser;
import com.farm.doc.domain.FarmDocPower;
import com.farm.doc.domain.FarmDocPowerVO;


import java.util.List;

public interface FarmDocPowerInter {
    /**
     * 设置权限
     *
     * @param entices
     * @param user
     */
    public int insertFarmDocPowerEntity(List<FarmDocPower> entices,LoginUser user ,String userId);

    //public int insertFarmDocPowerEntity(FarmDocPower[] entices,LoginUser user ,String userId);
    /**
     * 获取权限
     *
     * @param userId
     */
    public List<FarmDocPowerVO> getPowersByUserId(String userId);

    /**
     * 通过当前查看用户及文档类型获取权限
     *
     * @param user
     * @param docType
     */
    public String getPowerByUserDocType(LoginUser user,String docType);
}
