package com.farm.doc.dao.impl;

import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.utils.HibernateSQLTools;
import com.farm.doc.dao.FarmDocPowerDaoInter;
import com.farm.doc.domain.FarmDocPower;

import com.farm.doc.domain.FarmDocPowerVO;
import com.farm.doc.domain.ex.TypeBrief;
import com.farm.parameter.FarmParameterService;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.math.BigInteger;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
@Repository
public class FarmDocPowerDao extends HibernateSQLTools<FarmDocPower> implements FarmDocPowerDaoInter {
    @Resource(name = "sessionFactory")
    private SessionFactory sessionFatory;
    public SessionFactory getSessionFatory() {
        return sessionFatory;
    }
    public void setSessionFatory(SessionFactory sessionFatory) {
        this.sessionFatory = sessionFatory;
    }
    @Override
    protected SessionFactory getSessionFactory() {
        return sessionFatory;
    }
    @Override
    protected Class<FarmDocPower> getTypeClass() {
        return FarmDocPower.class;
    }

    /**
     * 删除一个实体
     *
     * @param entity 实体
     */
    @Override
    public void deleteEntity(FarmDocPower entity) {
        Session session = sessionFatory.getCurrentSession();
        session.delete(entity);
    }

    /**
     * 删除多个实体
     *
     * @param rules 实体
     */
    @Override
    public void deleteEntitys(List<DBRule> rules) {
        deleteSqlFromFunction(sessionFatory.getCurrentSession(), rules);
    }

    /**
     * 由id获得一个实体
     *
     * @param id
     * @return
     */
    @Override
    public FarmDocPower getEntity(String id) {
        Session session = sessionFatory.getCurrentSession();
        return (FarmDocPower) session.get(FarmDocPower.class, id);
    }

    /**
     * 插入一条数据
     *
     * @param entity
     */
    @Override
    public int insertEntity(FarmDocPower entity) {
        Session session = sessionFatory.getCurrentSession();
        try {
            session.save(entity);
            return 1;
        }
        catch (Exception e) {
            return 0;
        }

    }

    @Override
    public String getPowerByUserDocType(String loginUserId, String docType) {
        Session session = sessionFatory.getCurrentSession();
        SQLQuery sqlquery = session.createSQLQuery("select POWER from farm_doc_power where USER=? and DOCTYPE=?");
        sqlquery.setString(0, loginUserId);
        sqlquery.setString(1, docType);
        List<String> ts= sqlquery.list();

        System.out.println(ts);

        if(ts==null || ts.isEmpty()) {
            System.out.println("0");
            return "0";
        }
        else {
            System.out.println("1");
            return ts.get(0);
        }
    }

    /**
     * 通过userId获取用户权限
     *
     * @param userId return 返回权限集
     */
    @Override
    public List<FarmDocPowerVO> getEnticesByUserId(String userId) {
        Session session = sessionFatory.getCurrentSession();
        SQLQuery sqlquery = session.createSQLQuery("select fd.id,fd.parentid,fd.name,IFNULL(fdp.power,'0') as power ,IFNULL(fdp.user,'') as user from farm_doctype fd left join farm_doc_power fdp on fd.id=fdp.doctype and fdp.user=?  where fd.pstate='1'");
        sqlquery.setString(0, userId);
        //将普通对象转换成hibernate对象
        List<FarmDocPowerVO> o=  sqlquery.setResultTransformer(Transformers.aliasToBean(FarmDocPowerVO.class)).list();
        for (FarmDocPowerVO t:
             o) {
            System.out.println(t.getName());
        }

        return o;
    }



}
