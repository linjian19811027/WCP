package com.farm.doc.dao;

import com.farm.core.sql.query.DBRule;
import com.farm.doc.domain.FarmDocPower;
import com.farm.doc.domain.FarmDocPowerVO;

import java.util.List;

public interface FarmDocPowerDaoInter {
    /**
     * 删除一个实体
     *
     * @param entity
     *
     */
    public void deleteEntity(FarmDocPower entity);
    /**
     * 删除多个实体
     *
     * @param rules
     *
     */
    public void deleteEntitys(List<DBRule> rules);
    /**
     * 由id获得一个实体
     *
     * @param id
     * @return 实体
     */
    public FarmDocPower getEntity(String id);

    /**
     * 插入一条数据
     *
     * @param entity
     * return 操作个数
     */
    public int insertEntity(FarmDocPower entity);



    /**
     * 通过userId获取用户权限
     *
     * @param userId
     * return 返回权限集
     */
    public List<FarmDocPowerVO> getEnticesByUserId(String userId);


    public String getPowerByUserDocType(String loginUserId, String docType);

}
