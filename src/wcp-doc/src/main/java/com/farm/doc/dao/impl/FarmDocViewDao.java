package com.farm.doc.dao.impl;

import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.utils.HibernateSQLTools;
import com.farm.doc.dao.FarmDocViewDaoInter;
import com.farm.doc.domain.Doc;
import com.farm.doc.domain.FarmDocPower;
import com.farm.doc.domain.FarmDocView;
import com.farm.doc.domain.FarmDocmessage;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class FarmDocViewDao extends HibernateSQLTools<FarmDocView> implements FarmDocViewDaoInter {
    @Resource(name = "sessionFactory")
    private SessionFactory sessionFatory;
    public SessionFactory getSessionFatory() {
        return sessionFatory;
    }
    public void setSessionFatory(SessionFactory sessionFatory) {
        this.sessionFatory = sessionFatory;
    }
    @Override
    protected SessionFactory getSessionFactory() {
        return sessionFatory;
    }
    @Override
    protected Class<FarmDocView> getTypeClass() {
        return FarmDocView.class;
    }


    public void deleteEntity(FarmDocView entity) {
        Session session=sessionFatory.getCurrentSession();
        session.delete(entity);
    }

    public FarmDocView getEntity(String id) {
        Session session= sessionFatory.getCurrentSession();
        return (FarmDocView)session.get(FarmDocView.class, id);
    }
    @Transactional
    public FarmDocView insertEntity(FarmDocView entity) {
        Session session= sessionFatory.getCurrentSession();
        session.save(entity);
        return entity;
    }

    @Override
    public List<FarmDocView> selectEntitys(List<DBRule> rules) {
        return selectSqlFromFunction(
                sessionFatory.getCurrentSession(), rules);
    }
}
