package com.farm.doc.dao;


import com.farm.core.sql.query.DBRule;
import com.farm.doc.domain.FarmDocView;

import java.util.List;

public interface FarmDocViewDaoInter {
    /** 删除一个实体
     * @param entity 实体
     */
    public void deleteEntity(FarmDocView entity) ;
    /** 由id获得一个实体
     * @param id
     * @return
     */
    public FarmDocView getEntity(String id) ;
    /** 插入一条数据
     * @param entity
     */
    public FarmDocView insertEntity(FarmDocView entity);

    public List<FarmDocView> selectEntitys(List<DBRule> rules);
}
