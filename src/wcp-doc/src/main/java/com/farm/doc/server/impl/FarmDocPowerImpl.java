package com.farm.doc.server.impl;

import com.farm.core.auth.domain.LoginUser;
import com.farm.core.sql.query.DBRule;
import com.farm.core.time.TimeTool;
import com.farm.doc.dao.FarmDocPowerDaoInter;
import com.farm.doc.domain.FarmDocPower;
import com.farm.doc.domain.FarmDocPowerVO;
import com.farm.doc.server.FarmDocPowerInter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
@Service
public class FarmDocPowerImpl implements FarmDocPowerInter {
    @Resource
    private FarmDocPowerDaoInter farmDocPowerDao;

    /**
     * 设置权限
     *
     * @param entices
     * @param user
     */
    @Override
    @Transactional
    //public int insertFarmDocPowerEntity(FarmDocPower[] entices, LoginUser user,String userId) {
    public int insertFarmDocPowerEntity(List<FarmDocPower> entices, LoginUser user,String userId) {
       //先把原来的删了
        try {
            int count=0;
            farmDocPowerDao.deleteEntitys(DBRule.addRule(new ArrayList<DBRule>(), "USER", userId, "="));
            //再来添加
            for (FarmDocPower entity : entices) {
                entity.setCuser(user.getId());
                entity.setCtime(TimeTool.getTimeDate14());
                entity.setCusername(user.getName());
                entity.setEuser(user.getId());
                entity.setEusername(user.getName());
                entity.setEtime(TimeTool.getTimeDate14());
                count=count+farmDocPowerDao.insertEntity(entity);
            }
            return count;
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
            return -1;
        }
    }

    /**
     * 获取权限集合
     *
     * @param userId
     */
    @Override
    @Transactional
    public List<FarmDocPowerVO> getPowersByUserId(String userId) {
      return   farmDocPowerDao.getEnticesByUserId(userId);
    }

    /**
     * 通过当前查看用户及文档类型获取权限
     *
     * @param user
     * @param docType
     */
    @Override
    @Transactional
    public String getPowerByUserDocType(LoginUser user, String docType) {
        return   farmDocPowerDao.getPowerByUserDocType(user.getId(),docType);
    }
}
