package com.farm.doc.domain;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity(name = "FarmDocPower")
@Table(name = "farm_doc_power")
public class FarmDocPower {
    private static final long serialVersionUID = 3911185369411888075L;

    @Id
    @GenericGenerator(name = "systemUUID", strategy = "uuid")
    @GeneratedValue(generator = "systemUUID")
    @Column(name = "ID", length = 32, insertable = true, updatable = true, nullable = false)
    private String id;

    //用户
    @Column(name = "USER", length = 32, nullable = false)
    private String user;
    //栏目
    @Column(name = "DOCTYPE", length = 32, nullable = false)
    private String doctype;
    //权限 0默认看标题 ， 1看简介 ， 2看全部
    @Column(name = "POWER", length = 1, nullable = false)
    private String power;

    @Column(name = "EUSER", length = 32, nullable = false)
    private String euser;
    @Column(name = "EUSERNAME", length = 64, nullable = false)
    private String eusername;
    @Column(name = "CUSER", length = 32, nullable = false)
    private String cuser;
    @Column(name = "CUSERNAME", length = 64, nullable = false)
    private String cusername;
    @Column(name = "ETIME", length = 16, nullable = false)
    private String etime;
    @Column(name = "CTIME", length = 16, nullable = false)
    private String ctime;

    public FarmDocPower() {
    }

    public FarmDocPower(String user, String doctype,String power ,String ctime, String etime, String cusername,
                       String cuser, String eusername, String euser) {

        this.user = user;
        this.doctype = doctype;
        this.power = power;
        this.ctime = ctime;
        this.etime = etime;
        this.cusername = cusername;
        this.cuser = cuser;
        this.eusername = eusername;
        this.euser = euser;

    }


    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getDoctype() {
        return doctype;
    }

    public void setDoctype(String doctype) {
        this.doctype = doctype;
    }

    public String getPower() {
        return power;
    }

    public void setPower(String power) {
        this.power = power;
    }

    public String getEusername() {
        return this.eusername;
    }

    public void setEusername(String eusername) {
        this.eusername = eusername;
    }

    public String getCuser() {
        return this.cuser;
    }

    public void setCuser(String cuser) {
        this.cuser = cuser;
    }

    public String getCusername() {
        return this.cusername;
    }

    public void setCusername(String cusername) {
        this.cusername = cusername;
    }

    public String getEtime() {
        return this.etime;
    }

    public void setEtime(String etime) {
        this.etime = etime;
    }

    public String getCtime() {
        return this.ctime;
    }

    public void setCtime(String ctime) {
        this.ctime = ctime;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEuser() {
        return this.euser;
    }

    public void setEuser(String euser) {
        this.euser = euser;
    }

}
