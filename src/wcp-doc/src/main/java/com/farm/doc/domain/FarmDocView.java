package com.farm.doc.domain;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity(name = "FarmDocView")
@Table(name = "farm_doc_view")
public class FarmDocView  implements java.io.Serializable {

    private static final long serialVersionUID = 1018269502805991324L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCtime1() {
        return ctime1;
    }

    public String getCtime2() {
        return ctime2;
    }

    public void setCtime2(String ctime2) {
        this.ctime2 = ctime2;
    }

    public String getCtime3() {
        return ctime3;
    }

    public void setCtime3(String ctime3) {
        this.ctime3 = ctime3;
    }

    public String getCtime4() {
        return ctime4;
    }

    public void setCtime4(String ctime4) {
        this.ctime4 = ctime4;
    }

    public void setCtime1(String ctime1) {
        this.ctime1 = ctime1;
    }

    public String getCuser() {
        return cuser;
    }

    public void setCuser(String cuser) {
        this.cuser = cuser;
    }

    public String getCusername() {
        return cusername;
    }

    public void setCusername(String cusername) {
        this.cusername = cusername;
    }

    public String getDocid() {
        return docid;
    }

    public void setDocid(String docid) {
        this.docid = docid;
    }

    @Id
    @GenericGenerator(name = "systemUUID", strategy = "uuid")
    @GeneratedValue(generator = "systemUUID")
    @Column(name = "ID", length = 32, insertable = true, updatable = true, nullable = false)
    private String id;

    @Column(name = "CUSER", length = 32, nullable = false)
    private String cuser;
    @Column(name = "CUSERNAME", length = 64, nullable = false)
    private String cusername;
    @Column(name = "DOCID", length = 64, nullable = false)
    private String docid;

    @Column(name = "CTIME1", length = 16, nullable = false)
    private String ctime1;

    @Column(name = "CTIME2", length = 16, nullable = false)
    private String ctime2;
    @Column(name = "CTIME3", length = 16, nullable = false)
    private String ctime3;
    @Column(name = "CTIME4", length = 16, nullable = false)
    private String ctime4;
}
