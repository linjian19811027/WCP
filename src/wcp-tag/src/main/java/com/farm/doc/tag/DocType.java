package com.farm.doc.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import com.farm.doc.domain.FarmDoctype;
import com.farm.doc.domain.ex.DocEntire;
import com.farm.doc.domain.ex.TypeBrief;
import com.farm.doc.server.FarmDocTypeInter;
import com.farm.util.spring.BeanFactory;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.List;


public class DocType extends TagSupport {
    private final static FarmDocTypeInter farmDocTypeManagerImpl = (FarmDocTypeInter) BeanFactory
            .getBean("farmDocTypeManagerImpl");
    private static final long serialVersionUID = 7635883063151892525L;

    @Override
    public int doEndTag() throws JspException {
        JspWriter jspw = this.pageContext.getOut();
        try {
            List<TypeBrief> types = farmDocTypeManagerImpl.getTypes();
            String menu = "<li class=\"active\"><a href=\"webtype/view{0}/Pub1.html\" title=\"{1}\" ><span class=\"glyphicon glyphicon-th\"></span>&nbsp;{1}</a></li>";



            for (TypeBrief node : types) {
                if (node.getParentid().equals("NONE")) {
                    List<FarmDoctype> farmDoctypes= farmDocTypeManagerImpl.getAllChildNode(node.getId());
                    if(farmDoctypes.size()>0)
                    {
                        jspw.println("<li class=\"active dropdown\">");
                        jspw.println("  <a href=\"#\" id=\"dropdownMenuLink"+node.getId()+"\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">");
                        jspw.println("      <span class=\"glyphicon glyphicon-th\"></span>&nbsp;"+ node.getName());
                        jspw.println("  </a>");
                        jspw.println("  <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuLink"+node.getId()+"\">");

                        for (FarmDoctype node1 : farmDoctypes) {
                            jspw.println("<a style=\"color:#428bca\" href=\"webtype/view"+node1.getId()+"/Pub1.html\">&nbsp;&nbsp;<span class=\"glyphicon glyphicon-th-large\"></span>"+node1.getName()+"</a><br>");
                        }

                        jspw.println("  </div>");
                        jspw.println("</li>");
                    }
                    else {
                        jspw.println(MessageFormat.format(menu, node.getId(), node.getName()));
                    }
                }
            }

           /* jspw.println(doc.getTexts().getText1()
                            + (doc.getTexts().getText2() != null ? doc
                            .getTexts().getText2() : "")
                            + (doc.getTexts().getText3() != null ? doc
                            .getTexts().getText3() : ""));
*/
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public int doStartTag() throws JspException {

        return 0;
    }
}
