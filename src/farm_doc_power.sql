/*
Navicat MySQL Data Transfer

Source Server         : 192.168.7.75
Source Server Version : 50562
Source Host           : 192.168.7.75:3306
Source Database       : wcp2op

Target Server Type    : MYSQL
Target Server Version : 50562
File Encoding         : 65001

Date: 2020-06-18 08:54:09
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for farm_doc_power
-- ----------------------------
DROP TABLE IF EXISTS `farm_doc_power`;
CREATE TABLE `farm_doc_power` (
  `ID` varchar(32) NOT NULL,
  `USER` varchar(32) DEFAULT NULL,
  `DOCTYPE` varchar(32) DEFAULT NULL,
  `POWER` varchar(1) DEFAULT NULL,
  `CTIME` varchar(16) NOT NULL,
  `ETIME` varchar(16) NOT NULL,
  `CUSERNAME` varchar(64) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `EUSERNAME` varchar(64) NOT NULL,
  `EUSER` varchar(32) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET FOREIGN_KEY_CHECKS=1;
