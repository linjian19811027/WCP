<%@ page language="java" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF" %>

<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'center'">
        <form id="dom_formPower">
            <input type="hidden" id="entity_id" name="id" value="${ids}">
            <ul class="doctypeUl">
                <c:forEach var="node" items="${types}">
                    <c:if test="${node.parentid=='NONE'}">
                        <li>
                            <h5 class="showLableType">
                                <!--设置下拉权限-->
                                <div class="form-group">
                                    <label class="title">${node.name}</label>
                                    <select style="width: 120px;border:solid #d3d3d3 1px;height:25px" name="${node.id}"
                                            id="${node.id}" val="${node.power}">
                                        <option value="">~请选择~</option>
                                        <option value="1">仅简介</option>
                                        <option value="2">可下载</option>
                                        <option value="3">可创建</option>
                                    </select>
                                </div>
                            </h5>
                            <ul>
                                <c:forEach var="node1" items="${types}">
                                    <c:if test="${node1.parentid==node.id}">
                                        <li>
                                            <h5 class="showLableType">

                                                <!--设置下拉权限-->
                                                <div class="form-group">
                                                    <label>${node1.name}</label>
                                                    <select name="${node1.id}"
                                                            style="width: 120px;border:solid #d3d3d3 1px;height:25px"
                                                            id="${node1.id}" val="${node1.power}">
                                                        <option value="">~请选择~</option>
                                                        <option value="1">仅简介</option>
                                                        <option value="2">可下载</option>
                                                        <option value="3">可创建</option>
                                                    </select>
                                                </div>
                                            </h5>
                                            <ul>
                                                <c:forEach var="node2" items="${types}">
                                                    <c:if test="${node2.parentid==node1.id}">
                                                        <li>
                                                            <h5 class="showLableType">

                                                                <!--设置下拉权限-->
                                                                <div class="form-group">
                                                                    <label>${node2.name}</label>
                                                                    <select name="${node2.id}"
                                                                            style="width: 120px;border:solid #d3d3d3 1px;height:25px"
                                                                            id="${node2.id}"
                                                                            val="${node2.power}">
                                                                        <option value="">~请选择~</option>
                                                                        <option value="1">仅简介</option>
                                                                        <option value="2">可下载</option>
                                                                        <option value="3">可创建</option>
                                                                    </select>
                                                                </div>
                                                            </h5>
                                                        </li>
                                                    </c:if>
                                                </c:forEach>
                                            </ul>
                                        </li>
                                    </c:if>
                                </c:forEach>
                            </ul>
                        </li>
                    </c:if>
                </c:forEach>
            </ul>
        </form>
    </div>


    <div data-options="region:'south',border:false">
        <div class="div_button" style="text-align: center; padding: 4px;">

            <a id="dom_set_entityPower" href="javascript:void(0)"
               iconCls="icon-save" class="easyui-linkbutton">确定</a>

            <a id="dom_cancle_formPower" href="javascript:void(0)"
               iconCls="icon-cancel" class="easyui-linkbutton"
               style="color: #000000;">取消</a>
        </div>
    </div>
</div>
<script type="text/javascript">
    var submitEditActionPower = 'webtype/edit.do';

    var submitFormPower;
    $(function () {
        //表单组件对象
        submitFormPower = $('#dom_formPower').SubmitForm({
            pageType: "2",
            // grid : gridUser,
            // currentWindowId : 'winUser'
        });
        //关闭窗口
        $('#dom_cancle_formPower').bind('click', function () {
            $('#winUser').window('close');
        });

        //提交修改数据
        $('#dom_set_entityPower').bind('click', function () {

            /*  var entity = {};
              //var entity=new Array();
              /!* $("#dom_formPower").find("select").each(function(){
                   // this.find("option:selected").val();
                   var se=$(this);
                   var text = se.val(); //获取option的text
                   var id=se.attr("id");
                   entity.push({"power":text,"docType":id,"user":$('#entity_id').val()});
               });*!/
              entity.power = "1";
              entity.doctype = "1";
              entity.user = "1";*/
            var postData = {};
            var fdp = new Array();

            //这种数组方式时候json提交
            // $("#dom_formPower").find("select").each(function () {
            //     // this.find("option:selected").val();
            //     var se = $(this);
            //     var text = se.val(); //获取option的text
            //     var id = se.attr("id");
            //     fdp.push({"power": text, "doctype": id, "user": $('#entity_id').val()});
            // });

            //这种看起来像数组其实是字符串拼接
            var ii = 0;
            $("#dom_formPower").find("select").each(function () {

                var se = $(this);
                var text = se.val(); //获取option的text
                var id = se.attr("id");
                var user = $('#entity_id').val();
                postData['fdp['+ii+'].power'] =text;
                postData['fdp['+ii+'].doctype'] = id;
                postData['fdp['+ii+'].user'] = user;
                ii=ii+1;
            });

            //postData.fdp = fdp;
            postData.postIds = $('#entity_id').val();

            //默认的contentType是application/x-www-form-urlencoded，如果使用json提交需要显示声明contentType并对提交的数据json化JSON.stringify
            $.ajax({
                url: submitEditActionPower,
                data: postData,
                type: "POST",
                //dataType: 'json',
                //contentType: 'application/json',
                //contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                async: true,
                cache: false,
                success: function (flag) {

                    flag = $.parseJSON(flag);
                    if (flag.count > -1) {
                        $.messager.confirm(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.SUCCESS_CLOSE_WINDOW,
                            function (r) {
                                if (r) {
                                    $('#winUser').window('close');
                                }
                            });
                    } else {

                    }
                }
                ,
                error: function (XMLHttpRequest, textStatus, errorThrown) {

                }
                ,
                beforeSend: function () {
                }
                ,
                complete: function () {
                }
            });
        });


    });

</script>