<%@ page language="java" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<PF:basePath/>">
    <title>用户浏览数据统计</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <jsp:include page="/view/conf/include.jsp"></jsp:include>
</head>
<body class="easyui-layout">
<div data-options="region:'north',border:false">
    <form id="searchUserForm">
        <table class="editTable">
            <tr>
                <td class="title">
                    名称:
                </td>
                <td>
                    <input name="CUSERNAME:=" type="text">
                </td>
                <td class="title">
                    时间:
                </td>
                <td>
                    <input name="CTIME:=" type="text">
                </td>
            </tr>

            <tr style="text-align: center;">
                <td colspan="6">
                    <a id="a_search" href="javascript:void(0)"
                       class="easyui-linkbutton" iconCls="icon-search">查询</a>
                    <a id="a_reset" href="javascript:void(0)"
                       class="easyui-linkbutton" iconCls="icon-reload">清除条件</a>
                </td>
            </tr>
        </table>
    </form>
</div>
<div data-options="region:'center',border:false">
    <table id="dataUserGrid">
        <thead>
        <tr>

            <th field="CUSERNAME" data-options="sortable:true" width="30">
                名字
            </th>

            <th field="CTIME" data-options="sortable:true" width="20">
                时间
            </th>
            <th field="COUNTS1" data-options="sortable:true" width="20">
                阅读次数
            </th>
            <th field="COUNTS2" data-options="sortable:true" width="20">
                创建次数
            </th>
        </tr>
        </thead>
    </table>
</div>
</body>
<script type="text/javascript">
    var url_searchActionUser = "doctype/queryview.do";//查询URL
    var title_windowUser = "用户浏览数据统计";//功能名称
    var gridUser;//数据表格对象
    var searchUser;//条件查询组件对象
    var toolBarUserView = [ {
        id : 'export',
        text : '导出',
        iconCls : 'icon-add',
        handler : exportUserView
    }];
    //表格导出开始，将json对象导出为CSV文件
    function JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel) {
        //如果JSONData不是对象，那么JSON。parse将解析对象中的JSON字符串
        var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData)
            : JSONData;

        var CSV = '';
        //在第一行或第一行设置报表标题
        CSV += ReportTitle + '\r\n\n';
        //此条件将生成标签/标头
        if (ShowLabel) {
            var row = "";
            //导出的标题
            row = "名字,时间,阅读次数,创建次数";
            //用换行符追加标签行
            CSV += row + '\r\n';
        }
        //第一个循环是提取每一行
        debugger;
        for (var i = 0; i < arrData.rows.length; i++) {
            var row = "";

            //获取需要导出的字段
            row = arrData.rows[i].CUSERNAME+","+arrData.rows[i].CTIME+","+arrData.rows[i].COUNTS1+","+arrData.rows[i].COUNTS2+",";

            row.slice(0, row.length - 1);

            //在每一行之后添加一个换行符
            CSV += row + '\r\n';
        }
        if (CSV == '') {
            alert("无数据");
            return;
        }
        //Generate a file name

        //这里我使用了当前时间用来为导出的文件命名


        //初始化您想要的csv或xls文件格式
        var uri = 'data:text/csv;charset=utf-8,\ufeff' +encodeURI(CSV);

        var link = document.createElement("a");
        link.href = uri;

        link.style = "visibility:hidden";
        link.download =   "123.csv";

        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    }
    function exportUserView()
    {
        var dg = $("#dataUserGrid");
        //获取所有行
        var rowTmp=dg.datagrid('getData');
        if (rowTmp == '')
            return;
        JSONToCSVConvertor(rowTmp, "激活统计", true);
    }
    $(function () {
        //初始化数据表格
        gridUser = $('#dataUserGrid').datagrid({
            url: url_searchActionUser,
            fit: true,
            fitColumns: true,
            'toolbar' : toolBarUserView,
            pagination: true,
            closable: true,
            checkOnSelect: true,
            striped: true,
            border: false,
            rownumbers: true,
            ctrlSelect: true
        },'toExcel','dg.xls');
        //初始化条件查询
        searchUser = $('#searchUserForm').searchForm({
            gridObj: gridUser
        });
    });


</script>
</html>