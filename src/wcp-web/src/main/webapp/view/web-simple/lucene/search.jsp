<%@ page language="java" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <base href="<PF:basePath/>"/>
    <title>检索-<PF:ParameterValue key="config.sys.title"/></title>
    <meta name="description"
          content='<PF:ParameterValue key="config.sys.mate.description"/>'>
    <meta name="keywords"
          content='<PF:ParameterValue key="config.sys.mate.keywords"/>'>
    <meta name="author"
          content='<PF:ParameterValue key="config.sys.mate.author"/>'>
    <meta name="robots" content="index,follow">
    <jsp:include page="../atext/include-web.jsp"></jsp:include>


    <link href="<PF:basePath/>/text/lib/tour/css/bootstrap-tour.css" rel="stylesheet" />
    <script src="<PF:basePath/>/text/lib/tour/js/bootstrap-tour.js"></script>

    <script type="text/javascript">



        $(function () {

            var tour = new Tour({
                steps: [
                    {
                        element: '#js',
                        title: '检索功能',
                        content: "这个功能可以随时回到检索页面！",
                        placement: 'bottom',
                        backdrop: true,

                    },
                    {
                        element: '#cjzs',
                        title: '创建知识',
                        content: "可以直接创建你想要创建的知识！",
                        placement: 'bottom',
                        backdrop: true,
                    },
                    {
                        element: '#yh',
                        title: '${USEROBJ.name}',
                        content: "这个功能很总要，点击你的用户名，可以看到你的所创建的知识！",
                        placement: 'left',
                        backdrop: true,
                    }
                    ,
                    {
                        element: '#zx',
                        title: '注销！',
                        content: '不想使用了就通过它退出吧！',
                        placement: 'left',
                        backdrop: true,
                    }
                    ,
                    {
                        element: '#jjs',
                        title: '检索！',
                        content: '在工作做碰到的问题就用这个检索一下吧，看别的同事是不是已经解决了！',

                        backdrop: true,
                    }
                ],

                template: "<div class='popover tour'><div class='arrow'></div><h3 class='popover-title'></h3><div class='popover-content'></div><div class='popover-navigation'><button class='btn btn-default' data-role='prev'><</button><span data-role='separator'>|</span><button class='btn btn-default' data-role='next'>></button><button class='btn btn-default' data-role='end'>启动知识的存储</button></div></div>",
            });

            // Initialize the tour
            tour.init();

            // Start the tour
             tour.start();

           /* intro = introJs();
            intro.setOptions({
                prevLabel: "<",
                nextLabel: ">",
                skipLabel: "跳过",
                doneLabel: "启动知识的存储",

                steps: [
                    {
                        element: '#js',
                        intro: '这个功能可以随时回到检索页面',
                        position: 'bottom'
                    },
                    {
                        element: '#cjzs',
                        intro: '可以直接创建你想要创建的知识',
                        position: 'bottom'
                    },
                    {
                        element: '#yh',
                        intro: '点进去可以看到更多的内容哦',
                        position: 'bottom'
                    }
                    ,
                    {
                        element: '#zx',
                        intro: '不想使用了就通过它退出吧！',
                        position: 'bottom'
                    }
                    ,
                    {
                        element: '#jjs',
                        intro: '在工作做碰到的问题就用这个检索一下吧，看别的同事是不是已经解决了！',
                        position: 'top'
                    }
                ],*/
               /* hints: [
                    {
                        element: '#js',
                        hint: "This is a tooltip.",
                        hintPosition: 'top-middle'
                    },
                    {
                        element: '#cjzs',
                        hint: 'More features, more fun.',
                        position: 'left'
                    },
                    {
                        element: '#yh',
                        hint: "<b>Another</b> step.",
                        hintPosition: 'top-middle'
                    },
                    {
                        element: '#zx',
                        hint: "<b>Another</b> step.",
                        hintPosition: 'top-middle'
                    },
                    {
                        element: '.jjs',
                        hint: "<b>Another</b> step.",
                        hintPosition: 'top-middle'
                    }
                ]*/

            //});
           // intro.onbeforechange(function (targetElement) {
           //     console.log(this)  // this._introItems[this._currentStep]
           // });
           // intro.oncomplete(function(targetElement){
           //     intro.exit();
           // });
          //  intro.start();
           /* Mousetrap.bind(["?"], function(e) {
                intro.start();
            });*/
        });

    </script>

</head>
<body>
<jsp:include page="../commons/head.jsp"></jsp:include>
<jsp:include page="../commons/superContent.jsp"></jsp:include>
<div class="containerbox">
    <div class="container ">
        <div style="margin-top: 20px;"></div>
        <div class="row column_box">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-sm-12 column_box  hidden-xs">
                        <img class=" center-block" src="text/img/1.jpg" alt="wcp"  height="100px"/>
                    </div>
                    <div class="col-sm-12 column_box">
                        <jsp:include page="commons/includeSearchForm.jsp"></jsp:include>
                    </div>
                </div>
                <div style="margin-top: 20px;"></div>
            </div>
            <div class="col-lg-3"></div>
        </div>
        <!-- /.row -->
    <%--    <div class="row ">
            <div class="col-sm-12 hidden-xs">
                <jsp:include page="commons/includeSearchTopDoc.jsp"></jsp:include>
    &lt;%&ndash;            <jsp:include page="../commons/includePubType.jsp"></jsp:include>&ndash;%&gt;
            </div>
          &lt;%&ndash;  <div class="col-sm-3 ">
                <jsp:include page="../commons/includeHotKnowsMin.jsp"></jsp:include>
            </div>&ndash;%&gt;
        </div>--%>
    </div>
</div>
<jsp:include page="../commons/footServer.jsp"></jsp:include>
<jsp:include page="../commons/foot.jsp"></jsp:include>
</body>
</html>