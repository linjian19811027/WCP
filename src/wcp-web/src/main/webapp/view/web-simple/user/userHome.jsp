<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<title>${USEROBJ.name}-<PF:ParameterValue key="config.sys.title" /></title>
<meta name="description"
	content='<PF:ParameterValue key="config.sys.mate.description"/>'>
<meta name="keywords"
	content='<PF:ParameterValue key="config.sys.mate.keywords"/>'>
<meta name="author"
	content='<PF:ParameterValue key="config.sys.mate.author"/>'>
<meta name="robots" content="noindex,nofllow">
<jsp:include page="../atext/include-web.jsp"></jsp:include>


    <link href="<PF:basePath/>/text/lib/tour/css/bootstrap-tour.css" rel="stylesheet" />
    <script src="<PF:basePath/>/text/lib/tour/js/bootstrap-tour.js"></script>

    <script type="text/javascript">



        $(function () {

            var tour = new Tour({
                steps: [
                    {
                        element: '#js',
                        title: '检索功能',
                        content: "这个功能可以随时回到检索页面！",
                        placement: 'bottom',
                        backdrop: true,

                    },
                    {
                        element: '#cjzs',
                        title: '创建知识',
                        content: "可以直接创建你想要创建的知识！",
                        placement: 'bottom',
                        backdrop: true,
                    },
                    {
                        element: '#yh',
                        title: '${USEROBJ.name}',
                        content: "这个功能很总要，点击你的用户名，可以看到你的所创建的知识！",
                        placement: 'left',
                        backdrop: true,
                    }
                    ,
                    {
                        element: '#zx',
                        title: '注销！',
                        content: '不想使用了就通过它退出吧！',
                        placement: 'left',
                        backdrop: true,
                    }
                    ,
                    {
                        element: '#jjs',
                        title: '检索！',
                        content: '在工作做碰到的问题就用这个检索一下吧，看别的同事是不是已经解决了！',

                        backdrop: true,
                    }
                ],

                template: "<div class='popover tour'><div class='arrow'></div><h3 class='popover-title'></h3><div class='popover-content'></div><div class='popover-navigation'><button class='btn btn-default' data-role='prev'><</button><span data-role='separator'>|</span><button class='btn btn-default' data-role='next'>></button><button class='btn btn-default' data-role='end'>启动知识的存储</button></div></div>",
            });


            tour.init();


            tour.start();


        });

    </script>
</head>
<body>
	<jsp:include page="../commons/head.jsp"></jsp:include>
	<jsp:include page="../commons/superContent.jsp"></jsp:include>
	<div class="containerbox">
		<div class="container ">
			<div class="row  column_box">
				<div class="col-md-12">
					<jsp:include page="../statis/commons/includeMyStatis.jsp"></jsp:include>
				</div>
			</div>
	<%--	<div class="row">
				<div class="col-sm-12 putServerBox"
					style="border-left: 0px solid #ccc;">
					<ul style="float: left;">
						<PF:IfParameterEquals key="config.sys.opensource" val="false">
							<li style='width: 100px; height: 60px;'><a
								style="text-align: center;"
								href="resume/view.do?userid=${userid}"><img
									src="text/img/userinfo.png" alt="个人档案" class="img-thumbnail"
									style="width: 48px; height: 48px;" /></a></li>
						</PF:IfParameterEquals>
					</ul>
				</div>
			</div>--%>
			<div class="row ">
				<div class="col-md-12">
					<div class="panel panel-default "
						style="background-color: #FCFCFA;">
						<div class="panel-body">
							<p>
								<jsp:include page="../operation/includeUserOperate.jsp"></jsp:include>
							</p>
							<div class="table-responsive">
								<hr style="margin: 8px;" />
								<c:if test="${type=='know'}">
									<jsp:include page="commons/includeUserKnows.jsp"></jsp:include>
								</c:if>
								<c:if test="${type=='file'}">
									<jsp:include page="commons/includeUserKnows.jsp"></jsp:include>
								</c:if>
								<c:if test="${type=='joy'}">
									<jsp:include page="commons/includeUserKnows.jsp"></jsp:include>
								</c:if>
								<c:if test="${type=='group'}">
									<jsp:include page="commons/includeUserGroups.jsp"></jsp:include>
								</c:if>
								<c:if test="${type=='audit'}">
									<jsp:include page="commons/includeUserMyAudit.jsp"></jsp:include>
								</c:if>
								<c:if test="${type=='audith'}">
									<jsp:include page="commons/includeUserMyAuditH.jsp"></jsp:include>
								</c:if>
								<c:if test="${type=='audito'}">
									<jsp:include page="commons/includeUserMyAuditO.jsp"></jsp:include>
								</c:if>
								<c:if test="${type=='usermessage'}">
									<jsp:include page="commons/includeUserMessage.jsp"></jsp:include>
								</c:if>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../commons/footServer.jsp"></jsp:include>
	<jsp:include page="../commons/foot.jsp"></jsp:include>
</body>
<script type="text/javascript">
	function knowBoxGoPage(page) {
		window.location = basePath
				+ "webuser/MyPubHome.do?userid=${id}&query.currentPage=" + page;
	}
</script>
</html>