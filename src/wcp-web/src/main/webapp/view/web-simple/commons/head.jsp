<%@ page language="java" pageEncoding="utf-8" %>
<%@page import="com.farm.web.constant.FarmConstant" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF" %>
<%@ taglib uri="/view/conf/farmdoc.tld" prefix="DOC" %>
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation"
     style="margin: 0px;">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse"
                data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
            <span class="icon-bar"></span> <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand"
           style="color: #ffffff; font-weight: bold; padding: 5px;"
           href="<DOC:defaultIndexPage/>"> <img
                src="<PF:basePath/>/text/img/logo.jpg" height="40" alt="hgtech"
                title="hgtech" align="middle"/>
        </a>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
            <%--            <PF:IfParameterEquals key="config.sys.opensource" val="true">
                            <li class="active"><a href="home/Pubindex.html"><span
                                    class="glyphicon glyphicon-home"></span>&nbsp;资讯</a></li>
                        </PF:IfParameterEquals>--%>

            <%--   <li class="active"><a href="webgroup/MyPubHome.html"><span
                     class="glyphicon glyphicon-tree-conifer"></span>&nbsp;小组</a></li>
             <li class="active"><a href="webstat/MyPubHome.html"><span
                     class="glyphicon glyphicon-stats"></span>&nbsp;荣誉</a></li>--%>
            <li class="active" id="js"><a href="websearch/MyPubHome.html"><span
                    class="glyphicon glyphicon-search"></span>&nbsp;检索</a></li>
                <li class="active" id="cjzs"><a href="webfile/add.do?typeid=&groupid="><span class="glyphicon glyphicon-th-large"></span>&nbsp;创建知识</a></li>
            <%--<li class="active" id="cjzs"><a href="webfile/add.do?typeid=${typeid}&groupid=${groupid}"><span class="glyphicon glyphicon-th-large"></span>&nbsp;创建知识</a></li>--%>
            <%--<li class="active dropdown">
                <a href="#" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="glyphicon glyphicon-th-large"></span>&nbsp;创建知识
                </a>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                    <a  href="webtype/view${node.id}/Pub1.html">&nbsp;&nbsp;<span class="glyphicon glyphicon-th-large"></span>Action</a><br>
                    <a  href="#">&nbsp;&nbsp;<span class="glyphicon glyphicon-th-large"></span>Another
                        action</a><br>
                    <a  href="#">&nbsp;&nbsp;<span class="glyphicon glyphicon-th-large"></span>Something else
                        here</a><br>
                </div>
            </li>

            <li class="active dropdown">
                <a href="webfile/add.do?typeid=${typeid}&groupid=${groupid}" id="dropdownMenuLink1"
                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="glyphicon glyphicon-th-large"></span>&nbsp;创建知识
                </a>

                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink1">
                    <a  style="color:#428bca"  href="#">&nbsp;&nbsp;<span class="glyphicon glyphicon-th-large"></span>Action1</a><br>
                    <a style="color:#428bca"   href="#">&nbsp;&nbsp;<span class="glyphicon glyphicon-th-large"></span>Another action2</a><br>
                    <a  style="color:#428bca"  href="#">&nbsp;&nbsp;<span class="glyphicon glyphicon-th-large"></span>Something else
                        here3</a><br>
                </div>
            </li>--%>
            <%--   <li  class="active dropdown-toggle" data-toggle="dropdown">
                   <a href="webfile/add.do?typeid=${typeid}&groupid=${groupid}"><span
                           class="glyphicon glyphicon-th-large"></span>&nbsp;创建知识</a>
               </li>
               <div class="dropdown-menu">
                   <a class="dropdown-item" href="#">Link 1</a>
                   <a class="dropdown-item" href="#">Link 2</a>
                   <a class="dropdown-item" href="#">Link 3</a>
               </div>

           <li  class="active dropdown-toggle" data-toggle="dropdown">
               <a href="webfile/add.do?typeid=${typeid}&groupid=${groupid}"><span
                       class="glyphicon glyphicon-th-large"></span>&nbsp;创建知识</a>
           </li>
           <div class="dropdown-menu">
               <a class="dropdown-item" href="#">Link 4</a>
               <a class="dropdown-item" href="#">Link 5</a>
               <a class="dropdown-item" href="#">Link 6</a>
           </div>--%>
            <DOC:docType></DOC:docType>
            <%--            <c:forEach var="node" items="${types}">
                            <c:if test="${node.parentid=='NONE'}">
                                <li class="active"><a href="webtype/view${node.id}/Pub1.html" title="${node.name}" class="glyphicon glyphicon-th"><span></span>&nbsp;${node.name}</a></li>
                            </c:if>
                        </c:forEach>--%>

            <%--      <li class="active"><a href="home/PubAbout.html"><span
                          class="glyphicon glyphicon-phone-alt"></span>&nbsp;联系方式</a></li>--%>

        </ul>
              <form action="websearch/PubDo.do" method="post"
                    id="lucenesearchFormId"
                    class="navbar-form navbar-left hidden-xs hidden-sm" role="search">
                  <div class="form-group">
                      <input type="text" name="word" id="wordId" value="${word}"
                             class="form-control input-sm" placeholder="查询">
                      <input
                          type="hidden" id="pageNumId" name="pagenum">
                  </div>
                  <button type="submit" class="btn btn-default btn-sm">检索</button>
                  <%--<jsp:include page="../operation/includeCreatOperate.jsp"></jsp:include>--%>
              </form>
        <ul class="nav navbar-nav navbar-right" style="margin-right: 10px;">
            <c:if test="${USEROBJ==null}">
                <li class="active"><a href="login/webPage.html"><span
                        class="glyphicon glyphicon glyphicon-user"></span>&nbsp;登录</a></li>
            </c:if>
            <c:if test="${USEROBJ!=null}">
                <li class="active" id="yh"><a href="webuser/MyPubHome.do"><span
                        class="glyphicon glyphicon-user"></span>&nbsp;${USEROBJ.name}</a></li>
                <li class="active" id="zx"><a href="login/webout.html"><span
                        class="glyphicon glyphicon-off"></span>&nbsp;注销</a></li>
            </c:if>
        </ul>
    </div>
    <!-- /.navbar-collapse -->
</div>
<script type="text/javascript">
    function luceneSearch(key) {
        $('#wordId').val(key);
        $('#lucenesearchFormId').submit();
    }

    function luceneSearchGo(page) {
        $('#pageNumId').val(page);
        $('#lucenesearchFormId').submit();
    }

    //-->
</script>