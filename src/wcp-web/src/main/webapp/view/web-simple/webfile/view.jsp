<%@ page language="java" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/view/conf/wda.tld" prefix="WDA" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <base href="<PF:basePath/>"/>
    <title>${file.name}-<PF:ParameterValue key="config.sys.title"/></title>
    <meta http-equiv="Content-Security-Policy" content="default-src 'self'; style-src 'self' 'unsafe-inline';script-src 'self' 'unsafe-eval' 'unsafe-inline';img-src  'self'  'unsafe-inline'  'unsafe-eval'  data:">

    <meta name="description"
          content='<PF:ParameterValue key="config.sys.mate.description"/>'>
    <meta name="keywords"
          content='<PF:ParameterValue key="config.sys.mate.keywords"/>'>
    <meta name="author"
          content='<PF:ParameterValue key="config.sys.mate.author"/>'>
    <meta name="robots" content="noindex,nofllow">
    <jsp:include page="../atext/include-web.jsp"></jsp:include>



    <script charset="utf-8" src="<PF:basePath/>text/lib/super-validate/validate.js"></script>
    <script charset="utf-8" src="<PF:basePath/>text/javascript/pdfobject.js"></script>

</head>
<body>
<jsp:include page="../commons/head.jsp"></jsp:include>
<div class="containerbox">
    <div class="container">
        <div class="row">
            <div class="col-md-4"></div>
        </div>
        <div class="row" style="margin-top: 70px;">

            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">


                                <!--普通pc的-->
                                <div id="example1"   style="display: none;height:800px" ></div>

                                <!--手机的的-->
                                <iframe ref="iframe" id="example2" width="100%" height="800px" frameborder="0" style="display: none;"></iframe>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<jsp:include page="../commons/footServer.jsp"></jsp:include>
<jsp:include page="../commons/foot.jsp"></jsp:include>
<script type="text/javascript">
    $(function(){
        var userAgentInfo = navigator.userAgent;
        var Agents = ["Android", "iPhone",
            "SymbianOS", "Windows Phone",
            "iPod", "iPad", "iPad Pro"];
        var flag = true;
        for (var v = 0; v < Agents.length; v++) {
            if (userAgentInfo.indexOf(Agents[v]) > 0) {
                flag = false;
            }
        }
        if(flag)
        {
            $('#example2').attr('height','1px');
            $("#example1").show();//这个url可以直接带到跳转？
            PDFObject.embed("${file.url}", "#example1");
        }
        else
        {
            $('#example1').attr('height','1px');
            $("#example2").show();//这个url可以直接带到跳转？
            $('#example2').attr('src','webdoc/viewM/ViewFile.do?file='+encodeURIComponent('<PF:basePath/>${file.url}'));
        }


    });
    //判断是不是pc
    // function isPC() {
    //     var userAgentInfo = navigator.userAgent;
    //     var Agents = ["Android", "iPhone",
    //         "SymbianOS", "Windows Phone",
    //         "iPod", "iPad", "iPad Pro"];
    //     var flag = true;
    //     for (var v = 0; v < Agents.length; v++) {
    //         if (userAgentInfo.indexOf(Agents[v]) > 0) {
    //             flag = false;
    //             break;
    //         }
    //     }
    //     return flag;
    // }

</script>
</body>
</html>