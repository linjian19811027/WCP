package com.farm.wcp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.alibaba.fastjson.JSON;
import com.aliyun.dingtalkoauth2_1_0.models.GetUserTokenRequest;
import com.aliyun.dingtalkoauth2_1_0.models.GetUserTokenResponse;
import com.dingtalk.api.DingTalkClient;
import com.farm.core.auth.domain.LoginUser;
import com.farm.wcp.util.Constant;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.farm.authority.FarmAuthorityService;
import com.farm.core.auth.exception.LoginUserNoExistException;
import com.farm.core.page.ViewMode;
import com.farm.doc.tag.DefaultIndexPageTaget;
import com.farm.parameter.FarmParameterService;
import com.farm.wcp.util.ThemesUtil;
import com.farm.web.WebUtils;
import com.farm.web.online.OnlineUserOpImpl;
import com.farm.web.online.OnlineUserOpInter;


import com.aliyun.dingtalkcontact_1_0.models.GetUserHeaders;
import com.aliyun.dingtalkoauth2_1_0.models.GetUserTokenRequest;
import com.aliyun.dingtalkoauth2_1_0.models.GetUserTokenResponse;
import com.aliyun.teaopenapi.models.Config;
import com.aliyun.teautil.models.RuntimeOptions;
import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.DingTalkClient;
import com.dingtalk.api.request.OapiGettokenRequest;
import com.dingtalk.api.request.OapiSnsGetuserinfoBycodeRequest;
import com.dingtalk.api.request.OapiUserGetbyunionidRequest;
import com.dingtalk.api.request.OapiV2UserGetRequest;
import com.dingtalk.api.response.OapiGettokenResponse;
import com.dingtalk.api.response.OapiSnsGetuserinfoBycodeResponse;
import com.dingtalk.api.response.OapiUserGetbyunionidResponse;
import com.dingtalk.api.response.OapiV2UserGetResponse;


import com.taobao.api.ApiException;


@RequestMapping("/login")
@Controller
public class LoginController extends WebUtils {
    private final static Logger log = Logger.getLogger(LoginController.class);

    @RequestMapping("/submit")
    public ModelAndView loginCommit(String name, String password, HttpServletRequest request, HttpSession session) {
        try {
            if (FarmAuthorityService.getInstance().isLegality(name, password)) {
                // 登陆成功
                // 注册session
                loginIntoSession(session, getCurrentIp(request), name);
                return ViewMode.getInstance().returnRedirectUrl("/frame/index.do");
            } else {
                // 登陆失败
                return ViewMode.getInstance().putAttr("message", "用户密码错误").returnModelAndView("frame/login");
            }
        } catch (LoginUserNoExistException e) {
            log.info("当前用户不存在");
            return ViewMode.getInstance().putAttr("message", "当前用户不存在").returnModelAndView("frame/login");
        }
    }

    /**
     * 进入登录页面
     *
     * @param session
     * @return
     */
    @RequestMapping("/webPage")
    public ModelAndView login(HttpSession session, HttpServletRequest request) {
        // 将登陆前的页面地址存入session中，为了登录后回到该页面中
        String url = request.getHeader("Referer");
        session.setAttribute(FarmParameterService.getInstance().getParameter("farm.constant.session.key.from.url"),
                url);
        return ViewMode.getInstance().returnModelAndView(ThemesUtil.getThemePath() + "/login");
    }

    private boolean teshuzhif(String password) {
        //数字
        String REG_NUMBER = ".*\\d+.*";
        //小写字母
        String REG_UPPERCASE = ".*[A-Z]+.*";
        //大写字母
        String REG_LOWERCASE = ".*[a-z]+.*";
        //特殊符号
        String REG_SYMBOL = ".*[~!@#$%^&*()_+|<>,.?/:;'\\[\\]＞＜‘“＆＼＃{}\"]+.*";
        if (password == null || password.length() < 8) return false;
        int i = 0;
        if (password.matches(REG_NUMBER)) i++;
        if (password.matches(REG_LOWERCASE)) i++;
        if (password.matches(REG_UPPERCASE)) i++;
        if (password.matches(REG_SYMBOL)) i++;
        if (i < 4) return false;
        return true;
    }

    /**
     * 提交登录请求
     *
     * @param session
     * @return
     */
    @RequestMapping("/websubmit")
    public ModelAndView webLoginCommit(String name, String password, HttpServletRequest request, HttpSession session) {
        try {
            if (FarmAuthorityService.getInstance().isLegality(name, password)) {
                // 登陆成功
                // 注册session
                loginIntoSession(session, getCurrentIp(request), name);
                String goUrl = null;
                if (!teshuzhif(password)) {
                    return ViewMode.getInstance().returnRedirectUrl("/webuser/editCurrentUserPwd.do");
                }
                if (goUrl == null) {
                    // 要去哪里
                    goUrl = (String) session.getAttribute(
                            FarmParameterService.getInstance().getParameter("farm.constant.session.key.go.url"));
                    session.removeAttribute(
                            FarmParameterService.getInstance().getParameter("farm.constant.session.key.go.url"));
                }
                if (goUrl == null) {
                    // 来自哪里
                    goUrl = (String) session.getAttribute(
                            FarmParameterService.getInstance().getParameter("farm.constant.session.key.from.url"));
                }
                if (goUrl != null && goUrl.indexOf("login/webPage") > 0) {
                    // 如果返回的是登录页面，则去默认首页
                    goUrl = null;
                }
                if (goUrl == null) {
                    // 默认页面
                    goUrl = "/" + DefaultIndexPageTaget.getDefaultIndexPage();
                }
                return ViewMode.getInstance().returnRedirectUrl(goUrl);
            } else {
                // 登陆失败
                return ViewMode.getInstance().putAttr("loginname", name).setError("用户密码错误")
                        .returnModelAndView(ThemesUtil.getThemePath() + "/login");
            }
        } catch (LoginUserNoExistException e) {
            log.info("当前用户不存在");
            return ViewMode.getInstance().putAttr("loginname", name).setError("当前用户不存在")
                    .returnModelAndView(ThemesUtil.getThemePath() + "/login");
        }
    }

    @RequestMapping("/webout")
    public ModelAndView weblogOut(String name, HttpSession session) {
        clearCurrentUser(session);
        //return ViewMode.getInstance().returnRedirectUrl("/" + DefaultIndexPageTaget.getDefaultIndexPage());
        return ViewMode.getInstance().returnRedirectUrl("login/webPage.html");
    }

    @RequestMapping("/page")
    public ModelAndView login(String name) {
        return ViewMode.getInstance().returnModelAndView("frame/login");
    }

    @RequestMapping("/out")
    public ModelAndView logOut(String name, HttpSession session) {
        clearCurrentUser(session);
        //return ViewMode.getInstance().returnRedirectUrl("/login/page.do");
        return ViewMode.getInstance().returnRedirectUrl("login/webPage.html");
    }

    /**
     * 将登陆信息写进session
     *
     * @param session
     * @param ip
     * @param loginName
     */
    private void loginIntoSession(HttpSession session, String ip, String loginName) {
        // 开始写入session用户信息
        setCurrentUser(FarmAuthorityService.getInstance().getUserByLoginName(loginName), session);
        setLoginTime(session);
        // 开始写入session用户权限
        setCurrentUserAction(FarmAuthorityService.getInstance().getUserAuthKeys(getCurrentUser(session).getId()),
                session);
        // 开始写入session用户菜单
        setCurrentUserMenu(FarmAuthorityService.getInstance().getUserMenu(getCurrentUser(session).getId()), session);
        // 写入用户上线信息
        OnlineUserOpInter ouop = null;
        ouop = OnlineUserOpImpl.getInstance(ip, loginName, session);
        ouop.userLoginHandle(FarmAuthorityService.getInstance().getUserByLoginName(loginName));
        // 记录用户登录时间
        FarmAuthorityService.getInstance().loginHandle(getCurrentUser(session).getId());
    }


    //钉钉扫码登陆
    @RequestMapping(value = "/auth")
    public ModelAndView getAccessToken(@RequestParam(value = "authCode") String authCode, HttpServletRequest request, HttpSession session) throws Exception {
        System.out.println("调用的auth");
        try {
            com.aliyun.dingtalkoauth2_1_0.Client client = authClient();
            GetUserTokenRequest getUserTokenRequest = new GetUserTokenRequest()
                    //应用基础信息-应用信息的AppKey,请务必替换为开发的应用AppKey
                    .setClientId(Constant.APPKEY)

                    //应用基础信息-应用信息的AppSecret，,请务必替换为开发的应用AppSecret
                    .setClientSecret(Constant.APPSECRET)
                    .setCode(authCode)
                    .setGrantType("authorization_code");
            GetUserTokenResponse getUserTokenResponse = client.getUserToken(getUserTokenRequest);
            //获取用户个人token
            String accessToken = getUserTokenResponse.getBody().getAccessToken();
            String mobile = getUserinfoMobile(accessToken);
            System.out.println(mobile);

            if (FarmAuthorityService.getInstance().isLegalityByTel(mobile)) {

                // 登陆成功
                // 注册session
                LoginUser u = FarmAuthorityService.getInstance().getUserByTel(mobile);
                loginIntoSession(session, getCurrentIp(request), u.getLoginname());
                String goUrl = null;

                if (goUrl == null) {
                    // 要去哪里
                    goUrl = (String) session.getAttribute(
                            FarmParameterService.getInstance().getParameter("farm.constant.session.key.go.url"));
                    session.removeAttribute(
                            FarmParameterService.getInstance().getParameter("farm.constant.session.key.go.url"));
                }
                if (goUrl == null) {
                    // 来自哪里
                    goUrl = (String) session.getAttribute(
                            FarmParameterService.getInstance().getParameter("farm.constant.session.key.from.url"));
                }
                if (goUrl != null && goUrl.indexOf("login/webPage") > 0) {
                    // 如果返回的是登录页面，则去默认首页
                    goUrl = null;
                }
                if (goUrl == null) {
                    // 默认页面
                    goUrl = "/" + DefaultIndexPageTaget.getDefaultIndexPage();
                }
                return ViewMode.getInstance().returnRedirectUrl(goUrl);
            } else {
                // 登陆失败
                return ViewMode.getInstance().putAttr("loginname", "用户密码错误").setError("用户密码错误")
                        .returnModelAndView(ThemesUtil.getThemePath() + "/login");
            }
        } catch (LoginUserNoExistException e) {
            log.info("当前用户不存在");
            return ViewMode.getInstance().putAttr("loginname", "当前用户不存在").setError("当前用户不存在")
                    .returnModelAndView(ThemesUtil.getThemePath() + "/login");
        } catch (Exception e) {
            e.printStackTrace();
            return ViewMode.getInstance().putAttr("loginname", e.getMessage()).setError(e.getMessage())
                    .returnModelAndView(ThemesUtil.getThemePath() + "/login");
        }
/*
        //获取企业应用token，通过企业应用token以及用户UnionId才能对应企业内部用户。
        DingTalkClient client1 = new DefaultDingTalkClient("https://oapi.dingtalk.com/gettoken");
        OapiGettokenRequest requestz = new OapiGettokenRequest();
        requestz.setAppkey(Constant.APPKEY);
        requestz.setAppsecret(Constant.APPSECRET);
        requestz.setHttpMethod("GET");
        OapiGettokenResponse responsez = client1.execute(requestz);
        String accessTokenqy = responsez.getAccessToken();
        System.out.println(accessTokenqy);
        //String corpId = getUserTokenResponse.getBody().getCorpId();
        //System.out.println(accessToken);
        //System.out.println(corpId);
        //return  getUserinfo(accessToken);

        return getUserDetailInfo(accessTokenqy, getUserByTheUnionId(accessTokenqy, getUserinfo(accessToken)));*/
    }

    public static com.aliyun.dingtalkcontact_1_0.Client contactClient() throws Exception {
        Config config = new Config();
        config.protocol = "https";
        config.regionId = "central";
        return new com.aliyun.dingtalkcontact_1_0.Client(config);
    }

    /**
     * 获取用户个人信息UnionId
     *
     * @param accessToken
     * @return
     * @throws Exception
     */
    public String getUserinfo(String accessToken) throws Exception {
        com.aliyun.dingtalkcontact_1_0.Client client = contactClient();
        GetUserHeaders getUserHeaders = new GetUserHeaders();
        getUserHeaders.xAcsDingtalkAccessToken = accessToken;
        //获取用户个人信息，如需获取当前授权人的信息，unionId参数必须传me
        //String me = JSON.toJSONString(client.getUserWithOptions("me", getUserHeaders, new RuntimeOptions()).getBody());
        //System.out.println(me);

        return client.getUserWithOptions("me", getUserHeaders, new RuntimeOptions()).getBody().getUnionId();
    }

    public String getUserinfoMobile(String accessToken) throws Exception {
        com.aliyun.dingtalkcontact_1_0.Client client = contactClient();
        GetUserHeaders getUserHeaders = new GetUserHeaders();
        getUserHeaders.xAcsDingtalkAccessToken = accessToken;
        //获取用户个人信息，如需获取当前授权人的信息，unionId参数必须传me
        //String me = JSON.toJSONString(client.getUserWithOptions("me", getUserHeaders, new RuntimeOptions()).getBody());
        //System.out.println(me);
        return client.getUserWithOptions("me", getUserHeaders, new RuntimeOptions()).getBody().getMobile();
    }

    /*
     * 获取用户个人信息Userid
     * @param accessToken
     * @return
     * @throws Exception
     */
    public String getUserByTheUnionId(String accessToken, String unionId) throws ApiException {
        DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/topapi/user/getbyunionid");
        OapiUserGetbyunionidRequest req = new OapiUserGetbyunionidRequest();
        req.setUnionid(unionId);
        OapiUserGetbyunionidResponse rsp = client.execute(req, accessToken);
        return rsp.getResult().getUserid();
    }

    public static com.aliyun.dingtalkoauth2_1_0.Client authClient() throws Exception {
        Config config = new Config();
        config.protocol = "https";
        config.regionId = "central";
        return new com.aliyun.dingtalkoauth2_1_0.Client(config);
    }

    /*
     * 获取用户个人信息Userid
     * @param accessToken
     * @return
     * @throws Exception
     */
    public String getUserDetailInfo(String accessToken, String userId) throws ApiException {

        DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/topapi/v2/user/get");
        OapiV2UserGetRequest req = new OapiV2UserGetRequest();
        req.setUserid(userId);
        req.setLanguage("zh_CN");
        OapiV2UserGetResponse rsp = client.execute(req, accessToken);
        return rsp.getBody();
    }
}
