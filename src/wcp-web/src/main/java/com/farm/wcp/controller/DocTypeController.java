package com.farm.wcp.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.farm.doc.domain.FarmDocPower;
import com.farm.doc.domain.FarmDocPowerAddVO;
import com.farm.doc.domain.FarmDocPowerVO;
import com.farm.doc.server.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import com.farm.core.auth.domain.LoginUser;
import com.farm.core.page.ViewMode;
import com.farm.core.sql.result.DataResult;
import com.farm.doc.domain.FarmDoctype;
import com.farm.doc.domain.ex.TypeBrief;
import com.farm.wcp.know.service.KnowServiceInter;
import com.farm.wcp.util.ThemesUtil;
import com.farm.web.WebUtils;

@RequestMapping("/webtype")
@Controller
public class DocTypeController extends WebUtils {
	@Resource
	private FarmDocgroupManagerInter farmDocgroupManagerImpl;
	@Resource
	private FarmFileManagerInter farmFileManagerImpl;
	@Resource
	private FarmDocManagerInter farmDocManagerImpl;
	@Resource
	private FarmDocRunInfoInter farmDocRunInfoImpl;
	@Resource
	private KnowServiceInter KnowServiceImpl;
	@Resource
	private FarmDocmessageManagerInter farmDocmessageManagerImpl;
	@Resource
	private FarmDocOperateRightInter farmDocOperateRightImpl;
	@Resource
	private FarmDocTypeInter farmDocTypeManagerImpl;

    @Resource
    private FarmDocPowerInter farmDocPowerImpl;
	/**
	 * 分类首页
	 * 
	 * @return
	 * @throws Exception
	 */

	@RequestMapping(value = "/view{typeid}/Pub{pagesize}", method = RequestMethod.GET)
	public ModelAndView types(@PathVariable("typeid") String typeid, HttpSession session, HttpServletRequest request,
			@PathVariable("pagesize") Integer pagenum) throws Exception {
		ViewMode mode = ViewMode.getInstance();
		if (typeid == null || typeid.isEmpty()) {
			typeid = "NONE";
		} else {
			mode.putAttr("type", farmDocTypeManagerImpl.getType(typeid));
		}
		if (pagenum == null) {
			pagenum = 1;
		}
		LoginUser user = getCurrentUser(session);
		mode.putAttr("typeid", typeid);
		List<FarmDoctype> typepath =farmDocTypeManagerImpl.getTypeAllParent(typeid);
		List<TypeBrief> types = farmDocTypeManagerImpl.getPopTypesForReadDoc(getCurrentUser(session));
		List<TypeBrief> typesons = farmDocTypeManagerImpl.getTypeInfos(getCurrentUser(session), typeid);
		DataResult docs = farmDocTypeManagerImpl.getTypeDocs(user, typeid, 10, pagenum);
		return mode.putAttr("types", types).putAttr("typesons", typesons).putAttr("typepath", typepath).putAttr("docs", docs)
				.returnModelAndView(ThemesUtil.getThemePath()+"/type/type");
	}

	/**
	 * 显示权限信息（修改或浏览时）
	 *
	 * @return
	 * @throws Exception
	 */

	@RequestMapping("/power")
    public ModelAndView power(String ids) {
        try {
            List<FarmDocPowerVO> types = farmDocPowerImpl.getPowersByUserId(ids);

            return ViewMode.getInstance()
                    .putAttr("types", types)
					.putAttr("ids", ids)
                    .returnModelAndView("authority/includeChooseTypesPower");


        } catch (Exception e) {
            e.printStackTrace();

            return ViewMode.getInstance().setError(e + e.getMessage())
                    .returnModelAndView("authority/includeChooseTypesPower");
        }
    }
    /**
     * 提交编辑数据,默认情况下springmvc是不能解析前端提交的json的需要配合fastjson等解析并在springmvc的配置里面加入mvc:annotation-driven配置节还要在控制层的参数前面加入@RequestBody，
	 * 但是这就出现了另外一个问题返回的数据反而不是json了,错了，不是返回的不是json，返回的就是json而且还是json对象，原来返回的反而不是json对象是json字符串，所以前端需要做一个转换，逼不得已改回了表单提交
     *
     * @return
     */
    @RequestMapping("/edit")
    @ResponseBody
    //public int editSubmit(HttpSession session,@RequestBody FarmDocPowerAddVO entity) {
	public Map<String, Object> editSubmit(HttpSession session, FarmDocPowerAddVO entity) {
		LoginUser user = getCurrentUser(session);
		int i= farmDocPowerImpl.insertFarmDocPowerEntity(entity.getFdp(),user,entity.getPostIds());
        return ViewMode.getInstance().putAttr("count", i).returnObjMode();
		//return
    }
}
